let rows = document.querySelector("tbody").children
let matrix = []
for (var i = 0; i < rows.length; i++) {
    matrix.push(rows[i].children)
}
/*
    [[1,2,3],
     [4,5,6],
     [7,8,9]]
     */
function paintAll() {
    erase();
    for (let i=0; i<matrix.length; i++) { // afegir codi
        for (let j=0; j<matrix[i].length; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function erase() {
    for (let i=0; i<matrix.length; i++) { // afegir codi
        for (let j=0; j<matrix[i].length; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "white";
        }
    }
}

function paintRightHalf() {
    erase();

    for (let i=0; i<matrix.length; i++) { // afegir codi
        for (let j = matrix[i].length-1; j>=matrix[i].length/2; j--) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }
}

function paintLeftHalf() {
    erase();

    for (let i=0; i<matrix.length; i++) { // afegir codi
        for (let j=0; j<matrix[i].length/2; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperHalf() {
    erase();

    for (let i=0; i<matrix.length/2; i++) { // afegir codi
        for (let j=0; j<matrix[i].length; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintLowerTriangle() {
    erase();

    for (let i=0; i<matrix.length; i++) { // afegir codi
        for (let j=0; j<i; j++) { // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintUpperTriangle() {
    erase();
    for (let i=0; i<matrix.length; i++) { // afegir codi
        for (let j=matrix[i].length-1; j>=i; j--) { // afegir codi
            // afegir codi
            matrix[i][j].style.backgroundColor = "red";
        }
    }

}

function paintPerimeter() {
    erase();
    for (let i=0; i<matrix.length; i++) {
        for (let j=0; j<matrix[i].length; j++) {
            if (i==0 || i == matrix.length-1 || j==0 || j==matrix[i].length-1){
            matrix[i][j].style.backgroundColor = "red";
            }
        }
    }
}

function paintCheckerboard() {
    erase();
    for (let i=0; i<matrix.length; i++) { // afegir codi
        for (let j=0; j<matrix[i].length; j++) { // afegir codi
            if (i%2==0 && j%2==0 || i%2==1 && j%2==1){
            matrix[i][j].style.backgroundColor = "red";
            }
        }
    }
}
function paintCheckerboard2() {
    erase();
    for (let i=0; i<matrix.length; i++) { // afegir codi
        for (let j=0; j<matrix[i].length; j++) { // afegir codi
            if (i%2==0 && j%2==1 || i%2==1 && j%2==0){
            matrix[i][j].style.backgroundColor = "red";
            }
        }
    }
}

function paintNeighbours() {
    let inputX = document.getElementById("inputX").valueAsNumber;
    let inputY = document.getElementById("inputY").valueAsNumber;
    /*
    for () {
        for () {
	 mes coses ...
	}
    }
    */
}

function countNeighbours(x,y) {
    let count = 0;
    /*
    for () {
        for () {
	 mes coses ...
	}
    }
    */
    return count;
}

function paintAllNeighbours() {
    /*
    for () {
        for () {
	    matrix[i][j].innerText = count;
	}
    }
    */
}